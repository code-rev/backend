import { IRequest } from "./.d";

export function extractTokenFromHeader(req: IRequest): string {
  const authorizationHeader = req.headers.authorization;
  if (!authorizationHeader) {
    return '';
  }
  const token = authorizationHeader.slice(7);
  return token;
}
