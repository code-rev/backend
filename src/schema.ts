import { ResolverContext } from "./.d";
import { buildSchemaSync, AuthChecker } from "type-graphql";
import UserResolver from "./resolvers/user";
import AuthResolver from "./resolvers/auth";
import LanguageResolver from "./resolvers/language";
import { PostResolver } from "./resolvers/post";
import CodeResolver from "./resolvers/code";

const authChecker: AuthChecker<ResolverContext> = async ({ context }, _) => {
  if (!context.req.auth) return false;
  return true;
};

const schema = buildSchemaSync({
  resolvers: [
    UserResolver,
    AuthResolver,
    LanguageResolver,
    PostResolver,
    CodeResolver,
  ],
  authChecker: authChecker,
  validate: true,
});

export default schema;
