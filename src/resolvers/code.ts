import {
  FieldResolver,
  Query,
  Resolver,
  Root,
  Mutation,
  Arg,
  Ctx,
  Authorized,
} from "type-graphql";
import Code from "../entities/code";
import Language from "../entities/language";
import Post from "../entities/post";
import { ResolverContext } from "../.d";

@Resolver(Code)
export default class CodeResolver {
  @Mutation(() => Code, { nullable: true })
  @Authorized()
  async createCode(
    @Arg("title", () => String) title: string,
    @Arg("content", () => String) content: string,
    @Arg("languageId", () => Number) languageId: number,
    @Ctx() { req }: ResolverContext
  ) {
    const post = Post.create({
      content,
      author: { id: req.auth?.identity },
    });
    await post.save();
    const code = Code.create({
      post: post,
      title,
      language: { id: languageId },
    });
    try {
      await code.save();
    } catch {
      post.remove();
      return null;
    }
    return code;
  }

  @Query(() => [Code])
  async getCodes() {
    return await Code.createQueryBuilder("codes")
      .innerJoin("codes.post", "posts")
      .orderBy("posts.createdAt", "DESC")
      .getMany();
  }

  @Query(() => Code, { nullable: true })
  async getCode(@Arg("id", () => Number) id: number) {
    return await Code.findOne({
      where: {
        post: { id },
      },
    });
  }

  @FieldResolver()
  async language(@Root() code: Code) {
    return await Language.findOne({ id: code.languageId }, { cache: 1000 });
  }

  @FieldResolver()
  async post(@Root() code: Code) {
    return await Post.findOne({ id: code.postId }, { cache: 1000 });
  }
}
