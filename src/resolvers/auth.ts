import User from "../entities/user";
import {
  Resolver,
  Mutation,
  ObjectType,
  Field,
  Arg,
  Authorized,
  Query,
  Ctx,
} from "type-graphql";
import { compareSync } from "bcrypt";
import { sign } from "jsonwebtoken";
import configurationOjbect from "../configurations";
import { ResolverContext } from "../.d";

@ObjectType()
class AuthResolverOutput {
  @Field()
  accessToken: string;

  @Field({ nullable: true })
  refreshToken?: string;
}

@Resolver()
export default class AuthResolver {
  @Mutation(() => AuthResolverOutput, { nullable: true })
  async login(
    @Arg("username", () => String) username: string,
    @Arg("password", () => String) password: string
  ) {
    const user = await User.createQueryBuilder("users")
      .where("LOWER(users.username) = LOWER(:username)", {
        username,
      })
      .getOne();
    if (!user) return null;
    if (!compareSync(password, user.password)) {
      return null;
    }
    const accessToken = sign(
      { identity: user.id, typ: "access" },
      configurationOjbect.accessSecretKey,
      { expiresIn: "30m" }
    );
    const refreshToken = sign(
      { identity: user.id, typ: "refresh" },
      configurationOjbect.refreshSecretKey,
      { expiresIn: "7d" }
    );
    return {
      accessToken,
      refreshToken,
    };
  }

  @Mutation(() => AuthResolverOutput, { nullable: true })
  @Authorized()
  async refreshLogin(@Ctx() { req }: ResolverContext) {
    if (req.auth?.type !== "refresh") return null;
    const accessToken = sign(
      { identity: req.auth.identity, typ: "access" },
      configurationOjbect.accessSecretKey,
      { expiresIn: "30m" }
    );
    return { accessToken };
  }

  @Query(() => User)
  @Authorized()
  async getMe(@Ctx() { req }: ResolverContext) {
    const user = await User.findOne(req.auth?.identity);
    return user;
  }
}
