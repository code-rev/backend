import User from "./../entities/user";
import { ResolverContext } from "../.d";
import Post from "./../entities/post";
import Code from "./../entities/code";
import {
  Resolver,
  Mutation,
  Ctx,
  Arg,
  FieldResolver,
  Root,
  Authorized,
} from "type-graphql";
import { getRepository } from "typeorm";
import Rating from "../entities/ratings";

@Resolver(Post)
export class PostResolver {
  @Authorized()
  @Mutation(() => Post, { nullable: true })
  async replyCode(
    @Arg("replyToId", () => Number) replyToId: number,
    @Arg("content", () => String) content: string,
    @Ctx() { req }: ResolverContext
  ) {
    const code = await Code.findOne({
      where: {
        post: { id: replyToId },
      },
    });
    if (!code) return null;
    const post = Post.create({
      content,
      author: { id: req.auth?.identity },
      replyTo: { id: code.postId },
    });
    await post.save();
    return post;
  }

  @Mutation(() => Post, { nullable: true })
  @Authorized()
  async votePost(
    @Arg("id", () => Number) id: number,
    @Arg("vote", () => String) vote: "+" | "-",
    @Ctx() { req }: ResolverContext
  ) {
    const post = await Post.findOne(id);
    if (!post) return null;
    if (post.authorId === req.auth?.identity) {
      return null;
    }
    const oldVote = await Rating.findOne({
      where: {
        post,
        user: { id: req.auth?.identity },
      },
    });
    if (oldVote) {
      const allowedUpdateTime = new Date(
        Number(oldVote.updatedAt) + 3 * 60 * 1000
      );
      if (allowedUpdateTime < new Date()) {
        return null;
      }
    }
    const newVote = Rating.create({
      post,
      vote,
      user: { id: req.auth?.identity },
    });
    await newVote.save();
    return post;
  }

  @FieldResolver()
  async author(@Root() post: Post) {
    const user = await User.findOne({ id: post.authorId }, { cache: 1000 });
    return user;
  }

  @FieldResolver()
  async replyTo(@Root() post: Post) {
    return await Post.findOne({ id: post.replyToId }, { cache: 1000 });
  }

  @FieldResolver()
  async answers(@Root() post: Post) {
    return await getRepository(Post).find({
      where: {
        replyTo: post,
      },
      order: {
        createdAt: "ASC",
      },
      cache: 1000,
    });
  }

  @FieldResolver()
  async rate(@Root() post: Post) {
    const query = await Rating.createQueryBuilder("ratings")
      .select("SUM(CASE WHEN vote = '+' THEN 1 ELSE -1 END)", "sum")
      .where({
        post: post,
      })
      .limit(1)
      .cache(true)
      .getRawOne();
    return query.sum || 0;
  }
}
