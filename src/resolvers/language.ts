import { getRepository } from "typeorm";
import Language from "./../entities/language";
import {
  Resolver,
  Mutation,
  Arg,
  Query,
  InputType,
  Field,
  FieldResolver,
  Root,
} from "type-graphql";
import Code from "./../entities/code";

@InputType()
class UpdateLanguageInput implements Partial<Language> {
  @Field({ defaultValue: undefined, nullable: true })
  title: string;

  @Field({ defaultValue: undefined, nullable: true })
  description: string;
}

@Resolver(Language)
export default class LanguageResolver {
  @Mutation(() => Language)
  async createLanguage(
    @Arg("title", () => String) title: string,
    @Arg("description", () => String) description: string
  ) {
    const newLanguage = await Language.create({
      title,
      description,
    }).save();
    return newLanguage;
  }

  @Query(() => [Language])
  async getLanguages() {
    return await Language.createQueryBuilder("languages")
      .innerJoin("languages.codes", "codes")
      .groupBy("languages.id")
      .orderBy('COUNT(codes."languageId")', "DESC")
      .getMany();
  }

  @Query(() => Language, { nullable: true })
  async getLanguage(@Arg("title", () => String) title: string) {
    return await Language.createQueryBuilder("languages")
      .where("LOWER(languages.title) = LOWER(:title)", {
        title,
      })
      .getOne();
  }

  @Mutation(() => Language, { nullable: true })
  async updateLanguage(
    @Arg("id", () => Number) id: number,
    @Arg("data") { title, description }: UpdateLanguageInput
  ) {
    const langauge = await Language.findOne({ id });
    if (!langauge) return null;
    if (typeof title !== "undefined") langauge.title = title;
    if (typeof description !== "undefined") langauge.description = description;
    await langauge.save();
    return langauge;
  }

  @FieldResolver()
  async codes(@Root() language: Language) {
    return await getRepository(Code).find({
      where: {
        language: language,
      },
      cache: 1000,
    });
  }

  @FieldResolver()
  async codesCount(@Root() language: Language) {
    return await getRepository(Code).count({
      where: {
        language: language,
      },
      cache: 1000,
    });
  }
}
