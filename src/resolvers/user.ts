import User from "./../entities/user";
import { Resolver, Query, Arg, Mutation } from "type-graphql";
// import { QueryFailedError } from "typeorm";
import { validate } from "class-validator";
import { UserInputError } from "apollo-server-express";

@Resolver(User)
export default class UserResolver {
  @Mutation(() => User)
  async registerUser(
    @Arg("email", () => String) email: string,
    @Arg("username", () => String) username: string,
    @Arg("password", () => String) password: string
  ) {
    const user = User.create({
      email,
      username,
      password,
    });
    const errors = await validate(user);
    if (errors.length > 0) {
      const fields: { [key: string]: string } = {};
      for (let error of errors) {
        const [errorKey] = Object.keys(error.constraints!);
        fields[error.property] = error.constraints![errorKey];
      }
      throw new UserInputError("Validation Error", { fields });
    }
    await user.save();
    return user;
  }

  @Query(() => User, { nullable: true })
  async getUser(@Arg("username") username: string) {
    const user = await User.findOne({ username });
    return user || null;
  }
}
