import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from "class-validator";
import User from "../entities/user";

@ValidatorConstraint({ async: true })
export class DoesUserExistConstraint implements ValidatorConstraintInterface {
  validate(value: string, args: ValidationArguments) {
    return User.getRepository()
      .findOne({ where: { [args.property]: value }, select: ["id"] })
      .then((user) => {
        if (user) return false;
        return true;
      });
  }

  defaultMessage(args: ValidationArguments) {
    return `Another user with the same "${args.property}" found.`;
  }
}

export function DoesUserExist(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: DoesUserExistConstraint,
    });
  };
}
