import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import dotEnvSafe from "dotenv-safe";
import { Secret } from "jsonwebtoken";
const env = dotEnvSafe.config({
  allowEmptyValues: true,
});

interface IConfigurationObject {
  accessSecretKey: Secret;
  refreshSecretKey: Secret;
  database: PostgresConnectionOptions;
}

const configurationOjbect: IConfigurationObject = {
  accessSecretKey: env.required.ACCESS_SECRET_KEY,
  refreshSecretKey: env.required.REFRESH_SECRET_KEY,
  database: {
    type: "postgres",
    host: env.required.DATABASE_HOST || "localhost",
    port: Number(env.required.DATABASE_PORT) || 5432,
    username: env.required.DATABASE_USERNAME,
    password: env.required.DATABASE_PASSWORD,
    database: env.required.DATABASE_NAME || "code_rev",
    logging: true,
  },
};

export default configurationOjbect;
