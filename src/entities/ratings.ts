import {
  Entity,
  BaseEntity,
  ManyToOne,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import User from "./user";
import Post from "./post";

@Entity("ratings")
export default class Rating extends BaseEntity {
  @ManyToOne(() => User, { nullable: false, primary: true })
  user: User;

  @ManyToOne(() => Post, (post) => post.votes, {
    nullable: false,
    primary: true,
  })
  post: Post;

  @Column({ enum: ["+", "-"], type: "char", length: 1 })
  vote: "+" | "-";

  @CreateDateColumn({type: "timestamp with time zone"})
  createdAt: Date;

  @UpdateDateColumn({type: "timestamp with time zone"})
  updatedAt: Date;
}
