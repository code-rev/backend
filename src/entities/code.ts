import {
  Entity,
  OneToOne,
  JoinColumn,
  RelationId,
  Column,
  ManyToOne,
  BaseEntity,
} from "typeorm";
import { ObjectType, Field } from "type-graphql";
import Post from "./post";
import Language from "./language";

@Entity("codes")
@ObjectType()
export default class Code extends BaseEntity {
  @OneToOne(() => Post, (post) => post.code, { primary: true })
  @JoinColumn()
  @Field(() => Post)
  post: Post;

  @RelationId((codes: Code) => codes.post)
  postId: number;

  @Column({ type: "varchar", length: 256 })
  @Field()
  title: string;

  @ManyToOne(() => Language, (language) => language.codes, { nullable: false})
  @Field(() => Language, { nullable: false })
  language: Language;

  @RelationId((code: Code) => code.language)
  languageId: number;
}
