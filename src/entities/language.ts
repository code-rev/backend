import { BaseEntity } from "./base";
import { Entity, Column, OneToMany, JoinColumn } from "typeorm";
import Code from "./code";
import { Field, ObjectType } from "type-graphql";

@Entity("languages")
@ObjectType()
export default class Language extends BaseEntity {
  @Column({ type: "varchar", length: 128, unique: true })
  @Field()
  title: string;

  @Column({ type: "text" })
  @Field()
  description: string;

  @OneToMany(() => Code, (code) => code.language)
  @JoinColumn()
  @Field(() => [Code], { nullable: true })
  codes: Code[];

  @Field(() => Number)
  codesCount: number;
}
