import User from "./user";
import Code from "./code";
import { BaseEntity } from "./base";
import {
  Entity,
  Column,
  ManyToOne,
  OneToOne,
  OneToMany,
  RelationId,
} from "typeorm";
import { ObjectType, Field } from "type-graphql";
import Rating from "./ratings";

@Entity("posts")
@ObjectType()
export default class Post extends BaseEntity {
  @Column({ type: "text" })
  @Field()
  content: string;

  @ManyToOne(() => User, (user) => user.posts, { nullable: false })
  @Field(() => User)
  author: User;

  @RelationId((post: Post) => post.author)
  authorId: number;

  @ManyToOne(() => Post, (post) => post.answers, { nullable: true })
  @Field()
  replyTo: Post;

  @RelationId((post: Post) => post.replyTo)
  replyToId: number;

  @OneToMany(() => Post, (post) => post.replyTo)
  @Field(() => [Post], { nullable: true })
  answers: Post[];

  @OneToOne(() => Code, (code) => code.post, { nullable: true })
  @Field(() => Code, { nullable: true })
  code: Code;

  @OneToMany(() => Rating, (rating) => rating.post)
  votes: Rating[];

  @Field(() => Number, { nullable: false })
  rate: number = 0;
}
