import { DoesUserExist } from './../validators/user';
import { Entity, Column, OneToMany } from "typeorm";
import { BaseEntity } from "./base";
import bcrypt from "bcrypt";
import { Field, ObjectType } from "type-graphql";
import Post from "./post";
import Rating from "./ratings";

@Entity("users")
@ObjectType()
export default class User extends BaseEntity {
  @Field()
  @Column({ type: "varchar", length: 64, unique: true, nullable: false })
  @DoesUserExist()
  email: string;

  @Field()
  @Column({ type: "varchar", length: 32, unique: true, nullable: false })
  @DoesUserExist()
  username: string;

  @Column({
    unique: false,
    nullable: false,
    transformer: {
      to(value: string) {
        return bcrypt.hashSync(value, 10);
      },
      from(value: string) {
        return value;
      },
    },
  })
  password: string;

  @OneToMany(() => Post, (post) => post.author)
  posts: Post[];

  @OneToMany(() => Rating, (rating) => rating.user)
  votes: Rating[];
}
