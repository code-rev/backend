import "reflect-metadata";
import express, { Response, NextFunction } from "express";
import { ApolloServer } from "apollo-server-express";
import createDatabaseConnection from "./database";
import schema from "./schema";
import { IRequest, JWTPayload } from "./.d";
import { decode, verify } from "jsonwebtoken";
import { extractTokenFromHeader } from "./utils";
import configurationOjbect from "./configurations";

async function main() {
  const dbConnection = await createDatabaseConnection();
  await dbConnection.synchronize();

  const app = express();

  let apolloServer = new ApolloServer({
    schema: schema,
    context: (req) => req,
  });

  app.use(
    apolloServer.graphqlPath,
    (req: IRequest, _: Response, next: NextFunction) => {
      const token = extractTokenFromHeader(req);
      if (!token) {
        return next();
      }
      const decodedToken = decode(token) as JWTPayload;
      if (!["access", "refresh"].includes(decodedToken.typ)) {
        return next();
      }
      try {
        if (decodedToken.typ === "access") {
          verify(token, configurationOjbect.accessSecretKey);
          req.auth = { type: "access", identity: decodedToken.identity };
          return next();
        } else if (decodedToken.typ === "refresh") {
          verify(token, configurationOjbect.refreshSecretKey);
          req.auth = { type: "refresh", identity: decodedToken.identity };
          return next();
        }
      } catch {
        return next();
      }
    }
  );
  apolloServer.applyMiddleware({ app });
  app.get("/", (_, res) => {
    res.send("Hello World!");
  });

  app.listen(3000, async () => {
    console.info("App started on http://127.0.0.1:3000");
  });
}

main();
