import path from "path";
import { createConnection } from "typeorm";
import configurationOjbect from "./configurations";

const createDatabaseConnection = async () =>
  await createConnection({
    ...configurationOjbect.database,
    entities: [path.join(__dirname, "entities/*.ts")],
  });

export default createDatabaseConnection;
