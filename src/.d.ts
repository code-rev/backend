import { Request } from "express";
import User from "./entities/user";

interface IRequest extends Request {
  auth?: {
    type: "refresh" | "access";
    identity: number;
    user?: User;
  } | null;
}

interface JWTPayload {
  identity: number;
  typ: "refresh" | "access";
}

interface ResolverContext {
  req: IRequest;
}
